package com.example.piyush.facebookfriends.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.piyush.facebookfriends.R;


public class DetailsActivity extends ActionBarActivity {
    TextView firstname, lastname, gender, link;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        firstname = (TextView) findViewById(R.id.edit_firstname);
        lastname = (TextView) findViewById(R.id.edit_lastname);
        gender = (TextView) findViewById(R.id.edit_gender);
        link = (TextView) findViewById(R.id.edit_link);
        intent = getIntent();
        firstname.setText(intent.getStringExtra("firstname"));
        lastname.setText(intent.getStringExtra("lastname"));
        gender.setText(intent.getStringExtra("gender"));
        String url = "<a href='\"intent.getStringExtra(\"link\")\"'>Click Here</a>";
        link.setText(Html.fromHtml("<a href=\"" + intent.getStringExtra("link") + "\">Click here</a>"));
        link.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
