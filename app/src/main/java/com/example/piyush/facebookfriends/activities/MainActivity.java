package com.example.piyush.facebookfriends.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.piyush.facebookfriends.R;
import com.example.piyush.facebookfriends.adapters.TestAdapter;
import com.facebook.Session;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.piyush.facebookfriends.util.AppData.names;
import static com.example.piyush.facebookfriends.util.AppData.tagId;
import static com.example.piyush.facebookfriends.util.FacebookUtils.fbLogin;

public class MainActivity extends FragmentActivity {
    private final String APP_ID = "644982952280459";
    TestAdapter adapter;
    AsyncHttpClient clientMain, clientDetail;
    ListView listView;
    ProgressBar progress;
    String token = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        com.facebook.Settings.setApplicationId(APP_ID);
        listView = (ListView) findViewById(R.id.list);
        progress = (ProgressBar) findViewById(R.id.progress);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                click(parent, view, position, id);
            }
        });
    }

    private void click(AdapterView<?> parent, View view, int position, long id) {

        clientDetail = new AsyncHttpClient();
        progress.setVisibility(View.VISIBLE);
        String url = "https://graph.facebook.com/" + tagId.get(position) + "?access_token=" + token;
        clientDetail.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                try {
                    JSONObject details = new JSONObject(s);
                    String first_name = details.getString("first_name");
                    String last_name = details.getString("last_name");
                    String gender = details.getString("gender");
                    String link = details.getString("link");
                    Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                    intent.putExtra("firstname", first_name);
                    intent.putExtra("lastname", last_name);
                    intent.putExtra("gender", gender);
                    intent.putExtra("link", link);
                    progress.setVisibility(View.GONE);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                progress.setVisibility(View.GONE);
                Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle("Could Not Connect");
                dialog.show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getFriendList(String fbToken) {
        token = fbToken;
        progress.setVisibility(View.VISIBLE);
        clientMain = new AsyncHttpClient();
        clientMain.get(MainActivity.this,
                "https://graph.facebook.com/me/friends?access_token=" + token,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String s) {

                        try {
                            JSONObject data = new JSONObject(s);
                            JSONArray friends = data.getJSONArray("data");
                            for (int i = 0; i < friends.length(); i++) {
                                names.add(friends.getJSONObject(i).getString("name"));
                                tagId.add(friends.getJSONObject(i).getString("id"));
                            }
                            progress.setVisibility(View.GONE);
                            adapter = new TestAdapter(MainActivity.this, names);
                            listView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        progress.setVisibility(View.GONE);
                        Dialog dialog = new Dialog(MainActivity.this);
                        dialog.setTitle("Could Not Connect");
                        dialog.show();

                    }
                });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                fbLogin(0, MainActivity.this);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode,
                resultCode, data);
    }
}
