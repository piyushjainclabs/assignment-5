package com.example.piyush.facebookfriends.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.piyush.facebookfriends.R;

import java.util.ArrayList;

/**
 * Created by PIYUSH on 22-01-2015.
 */
public class TestAdapter extends BaseAdapter {
    Context ctx;
    private ArrayList<String> list = new ArrayList();

    public TestAdapter(Context ctx, ArrayList<String> data) {
        this.ctx = ctx;
        this.list = data;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.list_item, null);
        TextView exp = (TextView) row.findViewById(R.id.name);
        exp.setText(list.get(position).toString());
        return row;
    }
}
